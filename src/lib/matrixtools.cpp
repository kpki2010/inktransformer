/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "matrixtools.h"

#include <QDomDocument>
#include <QFile>
#include <QImage>
#include <QString>
#include <QStringList>

QMatrix4x4 MatrixTools::stringToMatrix( const QString &src )
{
    QStringList components = src.split( ";" );

    QVector< double > d( components.length() );
    for ( int i = 0; i < components.length(); i++ )
    {
        d[ i ] = components[ i ].toDouble();
    }

    if ( components.length() == 16 )
    {
        QMatrix4x4 result( d[  0 ], d[  1 ], d[  2 ], d[  3 ],
                           d[  4 ], d[  5 ], d[  6 ], d[  7 ],
                           d[  8 ], d[  9 ], d[ 10 ], d[ 11 ],
                           d[ 12 ], d[ 13 ], d[ 14 ], d[ 15 ] );
        return result;
    } else if ( components.length() == 9 )
    {
        QMatrix4x4 result( d[ 0 ], d[ 1 ], d[ 2 ], 0.0,
                           d[ 3 ], d[ 4 ], d[ 5 ], 0.0,
                           d[ 6 ], d[ 7 ], d[ 8 ], 0.0,
                           0.0,    0.0,    0.0,    1.0 );
        result.optimize();
        return result;
    }
    return QMatrix4x4();
}

QMatrix4x4 MatrixTools::positionInk( const QString &formDefinitionsFile, const QString &scannedImage )
{
    QRect formBoundary;
    QFile formDefinitions( formDefinitionsFile );
    if ( formDefinitions.open( QIODevice::ReadOnly ) )
    {
        QDomDocument doc;
        doc.setContent( formDefinitions.readAll() );
        QDomNodeList crs = doc.elementsByTagName( "ContentRect" );
        if ( crs.length() > 0 )
        {
            QDomElement contentRect = crs.at( 0 ).toElement();
            if ( !( contentRect.isNull() ) )
            {
                formBoundary = QRect( contentRect.attribute( "Left" ).toInt(),
                                      contentRect.attribute( "Top" ).toInt(),
                                      contentRect.attribute( "Width" ).toInt(),
                                      contentRect.attribute( "Height" ).toInt() );
            }
        }
        formDefinitions.close();
    }

    QImage image( scannedImage );

    QMatrix4x4 translation;
    translation.translate( formBoundary.left(), formBoundary.top() );

    QMatrix4x4 scale;
    if ( !( image.isNull() ) )
    {
        scale.scale( ( qreal ) image.width() / formBoundary.width(),
                     ( qreal ) image.height() / formBoundary.height() );
    }

    QMatrix4x4 result = translation * scale;
    result.optimize();
    return result;
}

QMatrix4x4 MatrixTools::matchingMatrix( const QString &matchingResults )
{
    QFile results( matchingResults );
    if ( results.open( QIODevice::ReadOnly ) )
    {
        QDomDocument doc;
        doc.setContent( results.readAll() );
        QDomElement homography = doc.elementsByTagName( "homography" ).at( 0 ).toElement();
        if ( !( homography.isNull() ) )
        {
            QVector< double > elements( 9 );
            QDomNodeList ats = homography.elementsByTagName( "at" );
            for ( uint i = 0; i < ats.length(); i++ )
            {
                QDomElement at = ats.item( i ).toElement();
                if ( !( at.isNull() ) )
                {
                    int r = at.attribute( "i" ).toInt();
                    int c = at.attribute( "j" ).toInt();
                    r--;
                    c--;
                    int cell = r * 3 + c;
                    if ( cell >= 0 && cell < 9 )
                    {
                        elements[ cell ] = at.text().toDouble();
                    }
                }
            }
            QMatrix4x4 result( elements[ 0 ], elements[ 1 ], elements[ 2 ], 0.0,
                               elements[ 3 ], elements[ 4 ], elements[ 5 ], 0.0,
                               elements[ 6 ], elements[ 7 ], elements[ 8 ], 0.0,
                               0.0,           0.0,           0.0,           1.0 );
            result.optimize();
            return result;
        }
    }
    return QMatrix4x4();
}
