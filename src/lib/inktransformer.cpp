/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "inktransformer.h"

#include <QDomDocument>
#include <QFile>

class InkTransformer::InkTransformerPrivate
{

public:

    QDomDocument contents;
    QMatrix4x4   matrix;

    InkTransformerPrivate( InkTransformer *transformer ) :
            contents( QDomDocument() ),
            matrix( QMatrix4x4() ),
            inkTransformer( transformer )
    {
    }

    virtual ~InkTransformerPrivate()
    {
    }

private:

    InkTransformer *inkTransformer;

};

InkTransformer::InkTransformer( QObject *parent ) :
    QObject( parent ),
    d( new InkTransformerPrivate( this ) )
{
}

InkTransformer::~InkTransformer()
{
    delete d;
}

const QMatrix4x4 &InkTransformer::matrix() const
{
    return d->matrix;
}

void InkTransformer::setMatrix( const QMatrix4x4 &m )
{
    d->matrix = m;
}

bool InkTransformer::setContent( const QByteArray &data, Format f )
{
    switch ( f )
    {

    case InkXML:
        {
            return d->contents.setContent( data );
        }
    }
    return false;
}

bool InkTransformer::loadFromFile( const QString &fileName, Format f )
{
    QFile file( fileName );
    if ( file.open( QIODevice::ReadOnly ) )
    {
        QByteArray data = file.readAll();
        file.close();
        return setContent( data, f );
    }
    return false;
}

const QString InkTransformer::toString( Format f ) const
{
    QDomDocument doc( d->contents );
    QDomNodeList points = doc.elementsByTagName( "Point" );
    for ( int i = 0; i < points.count(); i++ )
    {
        QDomElement point = points.at( i ).toElement();
        if ( !( point.isNull() ) )
        {
            double x = point.attribute( "x", "0.0" ).toDouble();
            double y = point.attribute( "y", "0.0" ).toDouble();
            QVector3D p( x, y, 1.0 );
            p = d->matrix.map( p );
            point.setAttribute( "x", p.x() );
            point.setAttribute( "y", p.y() );
        }
    }

    switch ( f )
    {
    case InkXML:
        {
            return doc.toString();
        }
    }

    return QString();
}

bool InkTransformer::saveToFile( const QString &fileName, Format f ) const
{
    QFile file( fileName );
    if ( file.open( QIODevice::WriteOnly ) )
    {
        file.write( toString( f ).toUtf8() );
        file.close();
        return true;
    }
    return false;
}
