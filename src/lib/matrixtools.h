/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATRIXTOOLS_H
#define MATRIXTOOLS_H

#include "inktransformerlib_config.h"

#include <QMatrix4x4>

class INK_TRANSFORMER_LIB_EXPORT MatrixTools
{

public:

    /**
      * @brief Convert a string representation of a matrix back to
      *        a matrix object.
      *
      * The format of the string is assumed to be d1;d2;d3;... where all d are
      * supposed to be convertable to double values. Each value is separated by
      * a semicolon. This method supports lists of 16 elements and 9 elements;
      * in case of 9 elements, the 4th column and rows are set to their default
      * (i.e. 4th identity vector, so when the matrix defined by the 3x3 elements is
      * the identify matrix, the completed 4x4 matrix will not transform the elements
      * it is applied on).
      * The elements are filled to the matrix row wise.
      */
    static QMatrix4x4 stringToMatrix( const QString &src );

    /**
      * @brief Create matrix that transforms an Ink file such that it fits its
      *        form definitions.
      *
      * This method takes the form definition (XML) file and an image file. The form definitions
      * are used to gather the expected size and location of the form contents. More specifically,
      * the form boundaries are read from this file.
      *
      * The image file is assumed to be the image, the ink file has been matched again. That means,
      * the image contains only the form contents (inside the form boundaries). The size of the
      * image is used as the seconds input parameter.
      *
      * The method will create a matrix that first moves the ink strokes such that their
      * very top left boundary lies at the top left point of the image definition. Additionally,
      * the ink is scaled such that the dimension of the defined form and the ink strokes fit.
      *
      * If applying this matrix to an Ink file should translate the strokes such that programs
      * can directly use the form definition file to query the (text) contents of the ink file
      * at the position of a given fields.
      */
    static QMatrix4x4 positionInk( const QString &formDefinitionsFile, const QString &scannedImage );

    /**
      * @brief Loads matching matrix from file.
      *
      * This method takes a matching result file and reads a matrix from this file;
      * applying the matrix will result in an ink file being transformed such that is
      * maps to a scanned version of the form.
      */
    static QMatrix4x4 matchingMatrix( const QString &matchingResults );


private:

    MatrixTools()
    {
    }

    MatrixTools( const MatrixTools & )
    {
    }
};

#endif // MATRIXTOOLS_H
