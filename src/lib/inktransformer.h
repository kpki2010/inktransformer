/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INKTRANSFORMER_H
#define INKTRANSFORMER_H

#include "inktransformerlib_config.h"

#include <QMatrix4x4>
#include <QList>
#include <QObject>

class INK_TRANSFORMER_LIB_EXPORT InkTransformer : public QObject
{

    Q_OBJECT

public:

    typedef enum
    {
        InkXML  = 0
    } Format;

    explicit InkTransformer( QObject *parent = 0 );
    virtual ~InkTransformer();

    const QMatrix4x4 &matrix() const;
    void setMatrix( const QMatrix4x4 &m );

    bool setContent( const QByteArray &data, Format f = InkXML );
    bool loadFromFile( const QString &fileName, Format f = InkXML );

    const QString toString( Format f = InkXML ) const;
    bool saveToFile( const QString &fileName, Format f = InkXML ) const;


signals:

public slots:

private:

    class InkTransformerPrivate;
    InkTransformerPrivate *d;

};

#endif // INKTRANSFORMER_H
