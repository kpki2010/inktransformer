TEMPLATE = lib
QT += core xml
CONFIG += plugin
TARGET = inktransformerlib
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release
DEFINES += INK_TRANSFORMER_LIB

HEADERS += \
    inktransformerlib_config.h \
    inktransformer.h \
    matrixtools.h

SOURCES += \
    inktransformer.cpp \
    matrixtools.cpp
