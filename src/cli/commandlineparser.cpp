/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "commandlineparser.h"

#include <QDebug>

const QString CommandLineParser::Input              = "-i;--input";
const QString CommandLineParser::Output             = "-o;--output";
const QString CommandLineParser::MatchingResults    = "-m;--matching-results";
const QString CommandLineParser::FormDefinitions    = "-f;--form-definitions";
const QString CommandLineParser::ScannedFile        = "-s;--scanned-file";
const QString CommandLineParser::Help               = "-h;--help;/?";

CommandLineParser::CommandLineParser( const QStringList &arguments ) :
        m_input( QString() ),
        m_output( QString() ),
        m_matchingFile( QString() ),
        m_formDefinitionFile( QString() ),
        m_scannedFile( QString() ),
        m_isHelpRequested( false ),
        m_gotNoInput( true ),
        m_options( QStringList() ),
        m_arguments( QStringList() ),
        m_descriptions( QStringList() ),
        m_maxOptionsLength( 0 ),
        m_maxArgumentsLength( 0 )
{
    m_gotNoInput = arguments.length() <= 1;
    int i = 1;
    while ( i < arguments.length() )
    {
        if ( Input.split( ";" ).contains( arguments[ i ] ) )
        {
            i++;
            m_input = readArgument( arguments, i );
        } else if ( Output.split( ";" ).contains( arguments[ i ] ) )
        {
            i++;
            m_output = readArgument( arguments, i );
        } else if ( MatchingResults.split( ";" ).contains( arguments[ i ] ) )
        {
            i++;
            m_matchingFile = readArgument( arguments, i );
        } else if ( FormDefinitions.split( ";" ).contains( arguments[ i ] ) )
        {
            i++;
            m_formDefinitionFile = readArgument( arguments, i );
        } else if ( ScannedFile.split( ";" ).contains( arguments[ i ] ) )
        {
            i++;
            m_scannedFile = readArgument( arguments, i );
        } else if ( Help.split( ";" ).contains( arguments[ i ] ) )
        {
            m_isHelpRequested = true;
        } else
        {
            m_isHelpRequested = true;
        }
        i++;
    }
}

CommandLineParser::~CommandLineParser()
{
}

QString CommandLineParser::readArgument(const QStringList &arguments, int index)
{
    if ( index < arguments.length() && index > 0 )
    {
        return arguments[ index ];
    } else
    {
        qWarning( QObject::tr( "Expected argument at index %1 but got end of argument list" ).arg( index ).toUtf8().data() );
        m_isHelpRequested = true;
        return QString();
    }
}

void CommandLineParser::printHelp()
{
    qDebug( "inktransformer - Transform Ink files" );
    qDebug();
    qDebug( " Usage: inktransformer [OPTIONS]" );
    qDebug();
    qDebug( "  Options:" );
    qDebug();
    printArgumentLine( Input, "F", "Read Ink strokes from file F" );
    printArgumentLine( Output, "F", "Write transformed Ink strokes to file F" );
    printArgumentLine( MatchingResults, "F", "Read matching matrix from matching results file F" );
    printArgumentLine( FormDefinitions, "F", "Use form definition file F to calculate additional transformations." );
    printArgumentLine( ScannedFile, "F", "Use image file F to calculate additional transformations." );
    printArgumentLine( Help, "", "Print this help and exit." );
    printArgumentLines();
    qDebug();
}

void CommandLineParser::printArgumentLine( const QString &args, const QString &parameters, const QString &description )
{
    m_options << args.split( ";" ).join( " " );
    m_arguments << parameters;
    m_descriptions << description;
    m_maxOptionsLength = m_options.last().length() > m_maxOptionsLength ? m_options.last().length() : m_maxOptionsLength;
    m_maxArgumentsLength = m_arguments.last().length() > m_maxArgumentsLength ? m_arguments.last().length() : m_maxArgumentsLength;
}

void CommandLineParser::printArgumentLines()
{
    for ( int i = 0; i < m_options.length(); i++ )
    {
        QString afterOptionFill;
        afterOptionFill.fill( QChar( ' ' ), 2 + m_maxOptionsLength - m_options[ i ].length() );

        QString afterArgumentsFill;
        afterArgumentsFill.fill( QChar( ' ' ), 2 + m_maxArgumentsLength - m_arguments[ i ].length() );
        qDebug( QString( "    %1%2%3%4%5" )
                  .arg( m_options[ i ] )
                  .arg( afterOptionFill )
                  .arg( m_arguments[ i ] )
                  .arg( afterArgumentsFill )
                  .arg( m_descriptions[ i ] ).toUtf8().data()
                  );
    }
}
