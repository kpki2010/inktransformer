TEMPLATE = app
TARGET = inktransformer
INCLUDEPATH += ../lib
QT += core gui
win32:CONFIG += console
CONFIG(debug, debug|release):LIBS += -L../../bin/Debug
else:LIBS += -L../../bin/Release
LIBS += -linktransformerlib
CONFIG(debug, debug|release):DESTDIR = ../../bin/Debug
else:DESTDIR = ../../bin/Release

SOURCES += \
    main.cpp \
    commandlineparser.cpp

HEADERS += \
    commandlineparser.h
