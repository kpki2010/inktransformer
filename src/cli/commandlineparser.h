/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include <QString>
#include <QStringList>

class CommandLineParser
{

public:

    static const QString Input;
    static const QString Output;
    static const QString MatchingResults;
    static const QString FormDefinitions;
    static const QString ScannedFile;
    static const QString Help;

    CommandLineParser( const QStringList &arguments );
    virtual ~CommandLineParser();

    void printHelp();

    const QString &input()
    {
        return m_input;
    }

    const QString &output()
    {
        return m_output;
    }

    const QString &matchingFile()
    {
        return m_matchingFile;
    }

    const QString &formDefinitionFile()
    {
        return m_formDefinitionFile;
    }

    const QString &scannedFile()
    {
        return m_scannedFile;
    }

    bool isHelpRequested()
    {
        return m_isHelpRequested;
    }

    bool gotNoInput()
    {
        return m_gotNoInput;
    }


private:

    QString         m_input;
    QString         m_output;
    QString         m_matchingFile;
    QString         m_formDefinitionFile;
    QString         m_scannedFile;
    bool            m_isHelpRequested;
    bool            m_gotNoInput;

    QStringList m_options;
    QStringList m_arguments;
    QStringList m_descriptions;
    int         m_maxOptionsLength;
    int         m_maxArgumentsLength;

    QString readArgument( const QStringList &arguments, int index );
    void printArgumentLine( const QString &args, const QString &parameters, const QString &description );
    void printArgumentLines();

};

#endif // COMMANDLINEPARSER_H
