/*
    inktransformer - Transform Digital Ink
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "commandlineparser.h"

#include "inktransformer.h"
#include "matrixtools.h"

#include <QCoreApplication>
#include <QDebug>
#include <QStringList>

int main( int argc, char **argv )
{
    QCoreApplication *app = new QCoreApplication( argc, argv );

    CommandLineParser clp( app->arguments() );
    if ( clp.isHelpRequested() || clp.gotNoInput() )
    {
        clp.printHelp();
    } else
    {
        InkTransformer it;
        QMatrix4x4 matchingMatrix;
        QMatrix4x4 positioningMatrix;
        if ( !( clp.input().isEmpty() ) )
        {
            it.loadFromFile( clp.input() );
        }
        if ( !( clp.matchingFile().isEmpty() ) )
        {
            matchingMatrix = MatrixTools::matchingMatrix( clp.matchingFile() );
        }
        if ( !( clp.formDefinitionFile().isEmpty() || clp.scannedFile().isEmpty() ) )
        {
            positioningMatrix = MatrixTools::positionInk( clp.formDefinitionFile(), clp.scannedFile() );
        }
        it.setMatrix( matchingMatrix * positioningMatrix );
        if ( !( clp.output().isEmpty() ) )
        {
            it.saveToFile( clp.output() );
        } else
        {
            qDebug( it.toString().toUtf8().data() );
        }
    }
    return 0;
}
